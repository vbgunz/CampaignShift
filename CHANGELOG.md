# CampainShift Change Log
## [0.0.15] - 2017-9-1
### Fixed
- First round of Scavenge having no gascans
- Survivors spawning away from the safe area

## [0.0.14] - 2017-27-12
### Fixed
- /sm_map & sm\_rcon changelevel replay spam (thanks kot4404)

## [0.0.13] - 2017-13-11
### Fixed
- All clients should now be able to long hold 'CROUCH' to vote

## [0.0.12] - 2017-26-10
### Fixed
- Campaigns are now by default cycled through 3x a day

## [0.0.11] - 2017-26-10
### Fixed
- Reverted to campaigns every 1440 minutes

## [0.0.10] - 2017-26-10
### Fixed
- Premature skipping of credits if voters left (thanks to Crasher_3637)

## [0.0.9] - 2017-23-10
### Changed
- Time per campaign is far more accurate now
- Campaigns are split up 1/3 a day (e.g., 13 at 36.92)

## [0.0.8] - 2017-20-10
### Added
- Replay voting
- By the minute rotation (e.g., 13 campaigns each at 110 minutes)

### Changed
- Refactored some variable names for explicitness

## [0.0.7] - 2017-18-10
### Changed
- Syntax conforms to #pragma newdecls required

### Added
- GetRealConnectedClients function to count all clients in game

## [0.0.6] - 2017-17-10
### Fixed
- Premature map switching if N clients disconnect during any map load

## [0.0.5] - 2017-16-10
### Added
- DisconnectHook to catch non-voting disconnections

### Changed
- Simplified SetupNextMap function

### Fixed
- Skipping of the last map in queue
- Maps not shifting due to disconnections that never voted

## [0.0.4] - 2017-30-08
### Fixed
- Coop mode due to Scavenge/Versus

## [0.0.3] - 2017-30-08
### Added
- Release ready support for campaign, versus and scavenge

## [0.0.2] - 2017-30-08
### Added
- Preliminary game mode support e.g., coop,versus,scavenge...

## [0.0.1] - 2017-30-08
### Added
- First Release

## This CHANGELOG follows http://keepachangelog.com/en/0.3.0/
### CHANGELOG legend

- Added: for new features.
- Changed: for changes in existing functionality.
- Deprecated: for once-stable features removed in upcoming releases.
- Removed: for deprecated features removed in this release.
- Fixed: for any bug fixes.
- Security: to invite users to upgrade in case of vulnerabilities.
- [YANKED]: a tag too signify a release to be avoided.
