# CampaignShift SourceMod Plugin

If you enjoy SourceMod and its community consider helping them them meet their monthly goal [here](http://sourcemod.net/donate.php). Your help, no matter the amount goes a long way in keeping a great project like SourceMod what it is... AWESOME.

Thanks :)

## License
CampaignShift a SourceMod L4D2 Plugin
Copyright (C) 2017  Victor "NgBUCKWANGS" Gonzalez

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

## About
CampaignShift changes the map automatically when players are done playing. Full support for campaign (including credits), versus and scavenge is implemented with no game breaking issues. At the end of a game, the next map (supporting the current mode) is loaded. Campaigns are played from top to bottom and each have their time partitioned by 24 hours, rinse and repeat. Players are offered an option to replay a campaign during the credit screen (see Important Notes). 

### Installing CampaignShift
1. Drop campaignshift.smx into its proper place
	- .../left4dead2/addons/sourcemod/plugins/campaignshift.smx
2. Load up CampaignShift
  - ```sm_rcon sm plugins load campaignshift```
  - OR restart the server
3. Customize CampaignShift (Generated on first load)
	- .../left4dead2/cfg/sourcemod/campaignshift.cfg

### Uninstalling CampaignShift
1. Remove .../left4dead2/addons/sourcemod/plugins/campaignshift.smx
2. Remove .../left4dead2/cfg/sourcemod/campaignshift.cfg

### Disabling CampaignShift
1. Move campaignshift.smx into plugins/disabled
2. ```sm_rcon sm plugins unload campaignshift``

## Customizing Maps
You'll need to know [how to compile a SourceMod plugin from the source](https://wiki.alliedmods.net/Compiling_SourceMod_Plugins). Once you're comfortable, find the ```g_cMappings``` array and add an 8 item line using double quotes and commas to separate the values. Here is an example of how to add Tanks Playground.

```"1", "0", "0", "0", "l4d2_tanksplayground", "l4d2_tanksplayground", "Tanks Playground", "Coop",```

Columns are **coop, versus, scavenge, survival, mapStart, mapEnd, campaignName, mapName**. Columns coop, versus, scavenge and survival require either ```0``` or ```1```. A ```0``` turns off support for the mode, a ```1``` will enable it. Although the campaign Tanks Playground supports versus, the map doesn't. The campaign does ship with **l4d2_tanksplayground_night** which is versus and to add that, we do

```"0", "1", "0", "0", "l4d2_tanksplayground_night", "l4d2_tanksplayground_night", "Tanks Playground", "Versus by Night",```

With that line added, you now have coop and versus support for Tanks Playground. The start and end maps not only have to already exist on the server but their written values are critical for a smooth experience. In the Tanks Playground example, the start and end maps are identical because it's only one map. Campaigns with multiple maps **require** the starting and ending maps to be different. If we had the following campaign.

### Campaign 'Wrecked' (map name / changelevel name)
1. Hi / ```l4d2_hi```
2. Smile / ```l4d2_smile```
3. Bye / ```l4d2_bye```

And if the campaign only supported both coop and versus but not scavenge or survival, we'd need this line

```"1", "1", "0", "0", "l4d2_hi", "l4d2_bye", "Wrecked", "Hi",```

### The Passing (Full Example)

```
"1", "1", "0", "1", "c6m1_riverbank", "c6m3_port", "The Passing", "Riverbank",
"0", "0", "1", "1", "c6m2_bedlam", "c6m2_bedlam", "The Passing", "Underground",
"0", "0", "1", "1", "c6m3_port", "c6m3_port", "The Passing", "Port",
```

### Important Notes
Every row in ```g_cMappings``` (except the last line) need to end with a comma. Support for Survival is added but currently they're meant to be endless and have no real ending. Some campaigns (e.g., The Parish) do not trigger the "finale_win" event and no notice for a replay vote will go up. On Campaigns like this, if you see the "Press Space Bar" notification on the bottom, you can hold your ```CROUCH``` key and tap ```SPACE BAR```. If you're alone, you'll repeat the map automatically. If you have company, you'll force the notification for a replay to appear for everyone.

## Thanks
Big thanks to ChrisP and their [ACS](https://forums.alliedmods.net/showthread.php?t=156392) plugin in which I've gotten a pretty clean list of maps and modes from. Big thanks to Lux for helping me break CampaignShift. If you want to find out how broken your work is, get Lux.

## Reaching Me
I love L4D2, developing, testing and running servers more than I like playing the game. Although I do enjoy the game and it is undoubtedly my favorite game, it is the community I think I love the most. It's always good to meet new people with the same interest :)

- [My Steam Profile](http://steamcommunity.com/id/buckwangs/)
- [My CampaignShift GitLab Page](https://gitlab.com/vbgunz/CampaignShift)
